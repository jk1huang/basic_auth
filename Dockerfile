FROM openjdk:11-slim

RUN mkdir /ws
COPY target/basic-auth-0.0.1-SNAPSHOT.jar /ws
WORKDIR /ws

CMD ["java","-jar","basic-auth-0.0.1-SNAPSHOT.jar"]

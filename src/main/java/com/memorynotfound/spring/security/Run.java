package com.memorynotfound.spring.security;

//import io.prometheus.client.Counter;
//import io.prometheus.client.Histogram;
//import io.prometheus.client.spring.boot.EnablePrometheusEndpoint;
//import io.prometheus.client.spring.boot.EnableSpringBootMetricsCollector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@ImportResource("classpath:spring-security-config.xml")
//@EnablePrometheusEndpoint
//@EnableSpringBootMetricsCollector
public class Run {

    public static void main(String[] args) {
        SpringApplication.run(Run.class, args);
    }
}

package com.memorynotfound.spring.security.entity;

import lombok.Data;

import javax.validation.constraints.Pattern;

@Data
public class User {

    @Pattern(regexp = "^(?=.*\\d).{4,8}$", flags = Pattern.Flag.UNICODE_CASE)
    private String username;


}

package com.memorynotfound.spring.security.web;

//import io.prometheus.client.Counter;
//import io.prometheus.client.Histogram;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class HomeController {

    @Value("${app.user.name}")
    private String appUserName;

    @Value("${app.user.name22}")
    private String appUserName22;

//    // Define a counter metric for /prometheus
//    static final Counter requests = Counter.build().name("xcal_requests_total").help("Total number of requests.").register();
//    // Define a histogram metric for /prometheus
//    static final Histogram requestLatency = Histogram.build()
//            .name("xcal_requests_latency_seconds").help("Request latency in seconds.").register();

    @GetMapping("/")
    public String greeting() {
        System.out.println("============ call home ctrl for second time =============");
        System.out.println("app.user.name: " + appUserName);
        //System.out.println("app.user.name22: " + appUserName22);
        Date date = new Date();
        System.out.println(date);
        return "Hello, World! 6\t\n";
    }

    @GetMapping("/hello1")
    public String hello1() {
        System.out.println("============ call hello1() =============");
        Date date = new Date();
        System.out.println(date);
//        requests.inc();
//        Histogram.Timer requestTimer = requestLatency.startTimer();
//        requestTimer.observeDuration();
        return "Hello, World! 3\t\n";
    }

    @GetMapping("/hello2")
    public String hello2() {
        System.out.println("============ call hello2() =============");
        Date date = new Date();
        System.out.println(date);
        try { Thread.sleep(1208 * 3 ); } catch (InterruptedException e) { }
        return "Hello, World! 3\t\n";
    }




}

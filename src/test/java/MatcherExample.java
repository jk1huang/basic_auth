import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatcherExample {

    public static void main(String[] args) {

        String text    =
                "This is the text to be searched " +
                        "for occurrences of the http:// pattern.";

        String patternString = ".*http://.*";

        Pattern pattern = Pattern.compile(patternString);

        Matcher matcher = pattern.matcher(text);
        boolean matches = matcher.matches();

        System.err.println(matches);

        String reg = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[^]{8,16}/$";

        String reg2 = "\\*[a-zA-Z0-9]{1}\\*";
        Pattern pattern2 = Pattern.compile(reg2);
        Matcher matcher2 = pattern2.matcher("aC2");
        boolean matches2 = matcher2.matches();
        System.err.println(matches2);

        String reg3 = "^[A-Za-z0-9._]{3,20}$";
        Pattern pattern3 = Pattern.compile(reg3);
        boolean matches3 = pattern3.matcher("8a1").matches();
        System.err.println(matches3);

        System.err.println(checkInput("9281a"));

    }


    private static boolean checkInput(String input) {
        Pattern pattern = Pattern.compile("^[A-Za-z0-9-]{5,20}$");
        Matcher m = pattern.matcher(input);
        if( !m.matches() ){ //匹配不到,說明輸入的不符合條件
            return false;
        }
        return true;
    }


}